//
//  AddDishViewController.swift
//  RestaurantDemo
//
//  Created by Cristian Olteanu on 27/03/2018.
//  Copyright © 2018 Cristian Olteanu. All rights reserved.
//

import UIKit
import RealmSwift

class AddDishViewController: UIViewController {

    @IBOutlet private weak var sectionSegment: UISegmentedControl!
    @IBOutlet private weak var nameTextField: UITextField!
    @IBOutlet private weak var priceTextField: UITextField!
    @IBOutlet private weak var descriptionTextView: UITextView!

    @IBAction func pressedClose(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func pressedAdd(_ sender: AnyObject) {
        guard !nameTextField.text!.isEmpty
            else {
                let alert = UIAlertController(title: "Name can't be empty", message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                present(alert, animated: true, completion: nil)

                return
        }
        guard !priceTextField.text!.isEmpty
            else {
                let alert = UIAlertController(title: "Price can't be empty", message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                present(alert, animated: true, completion: nil)

                return
        }
        guard !descriptionTextView.text!.isEmpty
            else {
                let alert = UIAlertController(title: "Description can't be empty", message: nil, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                present(alert, animated: true, completion: nil)

                return
        }

        dismiss(animated: true, completion: nil)
    }
}
