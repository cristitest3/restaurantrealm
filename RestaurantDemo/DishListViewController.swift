//
//  ViewController.swift
//  RestaurantDemo
//
//  Created by Cristian Olteanu on 14/03/2018.
//  Copyright © 2018 Cristian Olteanu. All rights reserved.
//

import UIKit
import SwiftRangeSlider
import RealmSwift

class DishListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var slider: RangeSlider!

    var allDishes: [Dish] = [Dish]()
    var filteredDishes: [Dish] = [Dish]()

    override func viewDidAppear(_ animated: Bool) {
        slider.lowerValue = 1.0
        slider.upperValue = 100.0
        let realm = try! Realm()
        allDishes = Array(realm.objects(Dish.self).sorted(byKeyPath: "name"))
        filteredDishes = allDishes
        tableView.reloadData()
    }

    @IBAction func sliderValueChanged(_ slider: RangeSlider) {
        let maxPrice = Int(slider.upperValue)
        let minPrice = Int(slider.lowerValue)

        filteredDishes = allDishes.filter { dish -> Bool in
            if dish.price <= maxPrice && dish.price >= minPrice {
                return true
            } else {
                return false
            }
        }
        tableView.reloadData()
    }

    @IBAction func pressedAddButton(_ sender: AnyObject) {
        let addDishViewController = storyboard?.instantiateViewController(withIdentifier: "addDish") as! AddDishViewController
        present(addDishViewController, animated: true, completion: nil)
    }

    // MARK: - UITableViewDataSource & UITableViewDelegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Dishes"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredDishes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dish = filteredDishes[indexPath.row]
        let dishCell = tableView.dequeueReusableCell(withIdentifier: "dishCell") as! DishTableViewCell
        
        dishCell.nameLabel.text = dish.name
        dishCell.dishImageView.image = UIImage.init(data: dish.imageData)
        dishCell.priceLabel.text = "price: \(dish.price)"

        return dishCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedDish = filteredDishes[indexPath.row]

        let dishDetailViewController = storyboard?.instantiateViewController(withIdentifier: "dishDetail") as! DishDetailViewController
        dishDetailViewController.dish = selectedDish

        navigationController?.pushViewController(dishDetailViewController, animated: true)
    }
}

